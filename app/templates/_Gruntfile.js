module.exports = function(grunt){
    'use strict';

    var TEMPLATES_PATH = 'templates/',
        TEMPLATES_EXTENSION = '.html';
    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        clean: {
            css : ['css'],
            js : ['js/jst.js', 'js/index.js']
        },

        jshint: { //TODO: review
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                node: true,
                globals: {
                    $: true,
                    _: true,
                    xit: true,
                    //document
                    window: false,
                    document: false,
                    // require.js
                    define: false,
                    // plug-ins
                    LocalFileSystem : false,
                    // mocha testing
                    mochaPhantomJS : false
                }
            },
            src: 'js/**/*.js'
        },

        browserify : {
            'js' : {
                browserifyOptions : {
                    debug : true
                },
                files : {
                    'js/index.js' : ['js/main.js']
                }
            },
        },

        'browserify-jst': {
            compile: {
                options: {
                    processName: function(filepath) {
                        var fn = filepath.substr(TEMPLATES_PATH.length);
                        fn = fn.substr(0, fn.length - TEMPLATES_EXTENSION.length);
                        return fn;
                    }
                },
                src: ["templates/**/*.html"],
                dest: "js/jst.js"
            }
        },


        compass: {
            dev: {
                options :{
                    sassDir: 'sass',
                    cssDir: 'css',
                    outputStyle: 'expanded',
                    relativeAssets: true,
                    imagesDir: 'img',
                    fontsDir: 'fonts'
                }
            }
        },

        watch: {
            css : {
                files: ['sass/**/*.scss'],
                tasks: ['compass:dev']
            }
        },
        serve : {
            options : {
                port : 8000
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-browserify-jst');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-serve');

    grunt.registerTask('build', [
        'clean:css',
        'clean:js',
        'jshint',
        'browserify-jst',
        'browserify',
        'compass:dev'
    ]);
};
