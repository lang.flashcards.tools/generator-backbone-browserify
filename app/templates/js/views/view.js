var Backbone = require('backbone'),
	jst = require('../jst');

module.exports = Backbone.View.extend({
	template : jst('view'),
	render : function(){
		this.$el.append(this.template({}));
		return this;
	}
});