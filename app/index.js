'use strict';
var util = require('util');
var path = require('path');
var generators = require('yeoman-generator');
var yosay = require('yosay');
var chalk = require('chalk');


var Html5BrowserifyGenerator = generators.Base.extend({

  init: function () {
    this.pkg = require('../package.json');

    /*this.on('end', function () {
      if (!this.options['skip-install']) {
        this.installDependencies();
      }
    });*/

    var me = this;
    /*this.on('method', function(method){
      console.log('METHOD:' + method);
    });*/
    // this.on('npmInstall:end', function(paths){
    //   me.log('npm installed with paths');
    //   me._installClientPackages();
    // });

    this.on('bowerInstall:end', function(paths){
      me.log('bower installed with paths');
    });
  },

  askFor: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay('Welcome to the Backbone-browserify generator!'));

    var parts = this.destinationRoot().split('/');
    var dirName = parts[parts.length - 1];
    var prompts = [
      {
        type : 'input',
        name : 'name',
        message : 'name: application package name ?',
        default : dirName
      },
      {
        type : 'input',
        name : 'description',
        message : 'description: ?',
        default : 'test application'
      },
      {
        type : 'input',
        name : 'version',
        message : 'package version ?',
        default : '0.0.0'
      },
      {
          type : 'input',
          name : 'gitignore',
          message : 'add .gitignore: ?[Yn]',
          default : 'Y'
      }//,
      // {
      //   type : 'checkbox',
      //   name : 'libraries',
      //   message : 'libraries to use:',
      //   choices : [
      //     { name : 'jQuery', value : 'jquery'},
      //     { name : 'Underscore', value : 'underscore'}
      //   ],
      //   default : ['jquery']
      // }
    ];

    this.prompt(prompts, function (props) {
      this._inputData = props;
      done();
    }.bind(this));
  },

  configuration : function(){
    var done = this.async();
    try{
    this.template('_package.json', 'package.json', this._inputData);
    this.copy('_Gruntfile.js', 'Gruntfile.js');
    done();
    }catch(e){
      console.error(e);
    }
  },

  resources: function(){
    try{
    this.template('index.html', 'index.html', this._inputData);
    this.mkdir('sass');
    this.copy('sass/index.scss', 'sass/index.scss');

    this.mkdir('js');
    this.copy('js/main.js', 'js/main.js');
    this.mkdir('js/views');
    this.copy('js/views/view.js', 'js/views/view.js');

    this.mkdir('templates');
    this.copy('templates/view.html', 'templates/view.html');
    if(this._inputData.gitignore.toUpperCase() === 'Y'){
        this.copy('_gitignore', '.gitignore');
    }
    }catch(e){
      console.error(e);
    }
  },

  installServerPackages : function(){
    this.npmInstall([],{});
  },

  // _installClientPackages : function(){
  //   this.bowerInstall([],{});
  // },

  _installNpms : function(){
    var me = this;
    this.npmInstall([],{},function(){
      me.log('npm packages installed');
    });
  }
});

module.exports = Html5BrowserifyGenerator;
